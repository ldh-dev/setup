#!/bin/bash

# Ensure we're up to date
echo "Updating..."
sudo apt-get update
sudo apt-get upgrade

# Add repositories
echo "Adding Enpass repo"
sudo sh -c 'echo "deb http://repo.sinew.in/ stable main" > /etc/apt/sources.list.d/enpass.list'
sudo apt-get update

# Installing required packages
echo "Installing packages"
sudo apt-get install -y enpass git vim atom curl zsh fonts-powerline

# Configure gitlab
echo "Configuring Gitlab"
git config --global user.name ldh-dev
git config --global user.email "fudjey+gitlab-dev@gmail.com"
echo "Configured username is...:"
git config --global user.name
echo "Configured email is...:"
git config --global user.email

# Configure ssh config
echo "Configuring SSH Config"
cat <<EOF > ~/.ssh/config
# Gitlab
Host gitlab.com
# RSAAuthentication yes
IdentityFile ~/.ssh/git_rsa
EOF

# Configure shell
echo "Installing oh-my-zsh"
grep sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="agnoster"/g' ~/.zshrc
export new_zsh_user_conf="\\
# User configuration\\
\\
prompt_context() {\\
if [[ "$USER" != "$DEFAULT_USER" || -n "$SSH_CLIENT" ]]; then\\
        prompt_segment black default "%(!.%{%F{yellow}%}.)$USER"\\
fi\\
}"
sed "s/# User configuration/${new_zsh_user_conf}/g"
zsh
