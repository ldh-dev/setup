#!/bin/bash

# Configure gitlab
echo "Configuring Gitlab"
git config --global user.name ldh-dev
git config --global user.email "fudjey+gitlab-dev@gmail.com"
echo "Configured username is...:"
git config --global user.name
echo "Configured email is...:"
git config --global user.email

echo "Creating gitroot"
mkdir ~/gitroot

# Configure ssh config
echo "Configuring SSH Config"
grep gitlab ~/.ssh/config || cat <<EOF > ~/.ssh/config
# Gitlab
Host gitlab.com
# RSAAuthentication yes
IdentityFile ~/.ssh/git_rsa
EOF

echo "Cloning config directory"
cd ~/gitroot
git clone git@gitlab.com:ldh-dev/config

echo "Installing packages from Brewfile"
brew bundle install ~/gitroot/config/Dotfiles/Brewfile

# Add VSCode to path
echo "Adding VSCode to path"
grep Visual\ Studio\ \Code.app ~/.zshrc || cat << EOF >> ~/.zshrc
export PATH="\$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
EOF

# Add VSCode extensions
echo "Installing vscode sync extension"
code --install-extension Shan.code-settings-sync
echo "Copying sync config"
cp ~/gitroot/config/Dotfiles/settings.json $HOME/Library/Application\ Support/Code/User/settings.json

# Configure shell
echo "Installing oh-my-zsh"
grep sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cp ~/gitroot/config/Dotfiles/zshrc ~/.zshrc